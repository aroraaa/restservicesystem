import requests
import git
import os
from radon.cli import CCHarvester, Config
from radon.complexity import SCORE

MANAGER = "http://127.0.0.1:45678"
ASKING_FOR_WORK = "http://127.0.0.1:45678/add_new_worker"


class Worker:

    cc_config = Config(
        exclude='', ignore='venu', order=SCORE, max='F', no_assert=True, show_closures=False, min='A'
    )

    def __init__(self):
        self.working = True
        self.worker_id, self.worker_directory_name = call_manager()
        self.worker_directory_path = "..\%s" % self.worker_directory_name

    def get_work_from_manger(self):
        while self.working:
            get_work_form_manager = requests.get(MANAGER)
            commit_number = get_work_form_manager.json()['commit_number']

            if commit_number is -1:
                self.working = False

            elif commit_number is not None:
                average_cc = self.perform_work(commit_number)
                requests.post(MANAGER, json={'average_cc': average_cc})

        print("Task completed")
        return self.worker_id

    def perform_work(self, commit):
        average_complexity_of_commit = 0
        complexity_of_commit = 0
        files = self.get_files_from_commit(commit)
        print files
        for files_in_commit in files:
            complexity_of_file = 0
            open_file = open(files_in_commit, 'r')
            cc = CCHarvester(files_in_commit, self.cc_config).gobble(open_file)

            for cc_result in cc:
                complexity_of_file += int(cc_result.complexity)

            complexity_of_commit += complexity_of_file
        if len(files) != 0:
            average_complexity_of_commit = complexity_of_commit / len(files)

        print "Average Complexity of commit from worker %s is %s" % (self.worker_id, average_complexity_of_commit)

        return complexity_of_commit

    def stop_worker(self):
        requests.post(ASKING_FOR_WORK)

    def get_files_from_commit(self, commit_number):
        repo = git.Repo(self.worker_directory_path).git
        repo.checkout(commit_number)
        files_from_commit = []
        for root, dirs, files in os.walk(self.worker_directory_path, topdown=True):
            for local_file in files:
                if local_file.endswith('.py'):
                    files_from_commit.append(root + '/' + local_file)
        return files_from_commit


def call_manager():
    response = requests.get(ASKING_FOR_WORK, json={'register_worker': True})
    id_of_worker = response.json()['worker_id']
    worker_directory_name = response.json()['directory']
    return id_of_worker, worker_directory_name


if __name__ == '__main__':
    worker = Worker()
    worker_id = worker.get_work_from_manger()
    print "Shutting down worker %s" % worker_id
    worker.stop_worker()
