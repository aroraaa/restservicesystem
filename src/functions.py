import os
import git

BITBUCKET_REPO_COMMITS = "https://github.com/python/buildmaster-config"


def clone_repository(directory_name):
    print "Cloning %s" % BITBUCKET_REPO_COMMITS

    path_of_directory = get_directory_path(directory_name)
    check_if_directory_exist(path_of_directory)

    if not os.listdir(path_of_directory):
        repo = git.Repo.clone_from(BITBUCKET_REPO_COMMITS, path_of_directory)
        print "Repository cloned"
    else:
        repo = git.Repo(path_of_directory)
        print "Getting repository"
    return repo


def get_directory_path(directory_name):
    directory_path = "..\%s" % directory_name
    return directory_path


def check_if_directory_exist(path_of_directory):
    if not os.path.exists(path_of_directory):
        os.mkdir(path_of_directory)
        print "Directory made successfully"
