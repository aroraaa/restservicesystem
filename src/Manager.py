import time
import os
import sys

from flask import Flask, request
from flask_restful import Api, Resource
import functions as fn

app = Flask(__name__)
api = Api(app)

RESULTS_FILE = "..\Results.txt"
MANAGER_DIRECTORY = "..\Manager"

NUM_OF_WORKERS_REQ = 0
NUM_OF_WORKER_AVAILABLE = 0
CURRENT_COMMIT_SEQUENCE = 0
LIST_OF_COMMITS_NUMBERS = []
LIST_OF_AVERAGE_CC = []
STARTING_TIME = 0
ENDING_TIME = 0
AVERAGE_CC = 0


class Manager(Resource):

    def get(self):

        global CURRENT_COMMIT_SEQUENCE, STARTING_TIME

        start_manager_response = self.get_starting_manager()
        if start_manager_response is True:
            STARTING_TIME = time.time()
            print "Starting manager"

        stop_manager_response = self.get_stop_manager_reponse()
        if stop_manager_response is True:
            stop_manager()

        check_number_of_workers_response = self.get_number_of_worker_check()
        if check_number_of_workers_response is True:
            response = self.check_if_anymore_commits_available()
            if response is True:
                current_commit_number = self.get_commit_number_to_send_to_worker()
                print "Sending commit number %s" % current_commit_number
                CURRENT_COMMIT_SEQUENCE += 1
                return {'commit_number': current_commit_number}
            else:
                print "No more commits available"
                return {'commit_number': -1}

        else:
            print "Waiting for all the workers to join"
            return {'commit_number': None}

    def post(self):
        global LIST_OF_AVERAGE_CC

        work_average_cc = request.get_json()['average_cc']
        LIST_OF_AVERAGE_CC.append(work_average_cc)
        finish_the_task_response = self.get_finishing_response()
        if finish_the_task_response is True:
            print "Finishing time %s" % ENDING_TIME

    def check_if_anymore_commits_available(self):
        if len(LIST_OF_COMMITS_NUMBERS) > CURRENT_COMMIT_SEQUENCE:
            return True
        else:
            return False

    def get_stop_manager_reponse(self):
        if len(LIST_OF_COMMITS_NUMBERS) <= CURRENT_COMMIT_SEQUENCE and NUM_OF_WORKER_AVAILABLE is 0:
            return True
        else:
            return False

    def get_starting_manager(self):
        global STARTING_TIME

        if CURRENT_COMMIT_SEQUENCE is 0:
            return True
        else:
            return False

    def get_number_of_worker_check(self):
        if NUM_OF_WORKERS_REQ is NUM_OF_WORKER_AVAILABLE:
            return True
        else:
            return False

    def get_commit_number_to_send_to_worker(self):
        commit_number = LIST_OF_COMMITS_NUMBERS[CURRENT_COMMIT_SEQUENCE]
        return commit_number

    def get_finishing_response(self):
        global ENDING_TIME

        if len(LIST_OF_AVERAGE_CC) is len(LIST_OF_COMMITS_NUMBERS):
            ENDING_TIME = time.time()
            return True
        else:
            return False


class NewWorker(Resource):

    def get(self):

        req_from_worker = request.get_json()['register_worker']

        if req_from_worker is True:
            print "Starting new worker"
            response = self.send_response_to_worker()
        else:
            response = {'worker_id': None, 'directory': None}
        return response

    def post(self):

        print "Stopping worker"
        self.stop_the_worker()

        stopping_manager = self.check_stopping_manager()
        if stopping_manager is True:
            stop_manager()

    def stop_the_worker(self):
        global NUM_OF_WORKER_AVAILABLE

        NUM_OF_WORKER_AVAILABLE -= 1

    def send_response_to_worker(self):
        global NUM_OF_WORKER_AVAILABLE

        worker_dir_name = "Worker %s" % NUM_OF_WORKER_AVAILABLE
        fn.clone_repository(worker_dir_name)
        send_respopnse_to_worker = {'worker_id': NUM_OF_WORKER_AVAILABLE, 'directory': worker_dir_name}
        NUM_OF_WORKER_AVAILABLE += 1
        return send_respopnse_to_worker

    def check_stopping_manager(self):
        if NUM_OF_WORKER_AVAILABLE is 0:
            return True
        else:
            return False


def stop_manager():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


def delete_the_directories():
    global NUM_OF_WORKERS_REQ, MANAGER_DIRECTORY

    if os.path.exists(MANAGER_DIRECTORY):
        os.rmdir(MANAGER_DIRECTORY)

    for i in range(NUM_OF_WORKERS_REQ):
        worker_directory_path = "..\Worker%s" % i
        if os.path.exists(worker_directory_path):
            os.rmdir(worker_directory_path)


def create_result_file():

    column_headings = "'NUM_OF_WORKERS', 'TOTAL_TIME_TAKEN', 'AVERAGE_CC'\n"
    with open(RESULTS_FILE, "a+") as results:
        data_of_results = results.read()
        if not data_of_results:
            results.write(str(column_headings))
        results.close()


def get_commit_numbers(repository):
    for commits in repository.iter_commits():
        LIST_OF_COMMITS_NUMBERS.append(str(commits))


def get_results():
    average_cc = sum(LIST_OF_AVERAGE_CC) / len(LIST_OF_AVERAGE_CC)

    return average_cc


def write_results_to_file(average_cc):

    time_taken = ENDING_TIME - STARTING_TIME

    data = "%s, %s, %s\n" % (NUM_OF_WORKERS_REQ, time_taken, average_cc)

    print(data)

    with open(RESULTS_FILE, "a+") as result_file:
        result_file.write(data)
        result_file.close()

    print "TIME TAKE %s" % time_taken
    delete_the_directories()


api.add_resource(Manager, '/')
api.add_resource(NewWorker, '/add_new_worker')


if __name__ == '__main__':
    NUM_OF_WORKERS_REQ = int(sys.argv[1])

    print "Starting Manager"
    repo = fn.clone_repository("Manager")

    create_result_file()

    get_commit_numbers(repo)

    app.run(debug=False, host='127.0.0.1', port=45678)

    results = get_results()

    write_results_to_file(results)

