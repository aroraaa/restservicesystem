# REST Service System
---
**Aashish Arora, B.A.I. Computer Engineering, 16338462**

## Dependencies :
These dependencies are specified in the requirements_to_compile.txt file. Installing these dependencies is a pre-requisite to launching the Manager and Worker nodes, and so the installation of the dependencies is included in the script manager_startup.sh.

flask

flask-restful

radon (library for calculating the cyclomatic complexity of python files)

GitPython

Instructions to Launch

Using the following shell scripts:
./launch_manager.sh 5_: This script will i) install the project dependencies, and ii) spawn a manager that will wait for 5 workers to spawn before distributing work.
./launch_worker.sh 5_: This script will spawn the specified number of workers.


## Results

Here: https://bitbucket.org/aroraaa/restservicesystem/src/c8951de06e09f172c68bf15c83ba90385509ce7c/chart.png?at=master&fileviewer=file-view-default

![graph](https://bitbucket.org/aroraaa/restservicesystem/src/c8951de06e09f172c68bf15c83ba90385509ce7c/chart.png?at=master&fileviewer=file-view-default)