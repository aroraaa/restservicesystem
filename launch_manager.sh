#!/bin/sh

# first install dependencies
pip install -r requirements.txt

# then launch manager: $1 is the number of workers
python src/Manager.py $1